{
    "id": "3536f3d4-87d2-4bad-92a9-60cb5518956a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4606794-5681-4807-9585-d1f94b49c354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3536f3d4-87d2-4bad-92a9-60cb5518956a",
            "compositeImage": {
                "id": "fe2e7d31-5faf-44ae-9fc1-ca0e3653c517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4606794-5681-4807-9585-d1f94b49c354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "265fcae4-997e-40ef-8dec-ccafe73c97a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4606794-5681-4807-9585-d1f94b49c354",
                    "LayerId": "d0ad43df-b9d3-47ff-8405-83da92e306ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d0ad43df-b9d3-47ff-8405-83da92e306ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3536f3d4-87d2-4bad-92a9-60cb5518956a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}