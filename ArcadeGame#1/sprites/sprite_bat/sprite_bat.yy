{
    "id": "47c431bb-1cd3-49c7-9683-15457bd7f2e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4a49503-f2ec-45c4-9c0a-dc95b13cb0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47c431bb-1cd3-49c7-9683-15457bd7f2e4",
            "compositeImage": {
                "id": "dcc703ef-d0ea-40df-8301-d5749b1154b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a49503-f2ec-45c4-9c0a-dc95b13cb0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08ef4a34-0bae-450f-a60e-99c7972d7d32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a49503-f2ec-45c4-9c0a-dc95b13cb0e6",
                    "LayerId": "54f876e9-c939-4c18-971d-09ac7b2d0574"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "54f876e9-c939-4c18-971d-09ac7b2d0574",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47c431bb-1cd3-49c7-9683-15457bd7f2e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}