{
    "id": "ea6d4dec-632f-48c3-9a13-dc846b9d0b70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 61,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "916a6ce9-bc0c-4d7e-aa61-fe29707f0c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea6d4dec-632f-48c3-9a13-dc846b9d0b70",
            "compositeImage": {
                "id": "7c9eba39-2d54-4c79-842d-0c8ae8e06848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916a6ce9-bc0c-4d7e-aa61-fe29707f0c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63a6fd7-d506-4fdf-bdf8-39b6c1c07e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916a6ce9-bc0c-4d7e-aa61-fe29707f0c77",
                    "LayerId": "dc878bb4-e2ae-46d2-a481-062d5a81ad78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dc878bb4-e2ae-46d2-a481-062d5a81ad78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea6d4dec-632f-48c3-9a13-dc846b9d0b70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}