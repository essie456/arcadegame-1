{
    "id": "df17f0a1-78bd-45b5-a54d-51ad29a525c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_brick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8e1992d-2fd4-47ff-bbb8-9709484123a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df17f0a1-78bd-45b5-a54d-51ad29a525c4",
            "compositeImage": {
                "id": "5933119f-a2ff-4f96-8b72-299b57ec5547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e1992d-2fd4-47ff-bbb8-9709484123a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709bc18b-6ab3-4a28-b9b8-3e05e54a65e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e1992d-2fd4-47ff-bbb8-9709484123a9",
                    "LayerId": "9ca02f3b-dc47-476a-9888-b3e79ee9689d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9ca02f3b-dc47-476a-9888-b3e79ee9689d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df17f0a1-78bd-45b5-a54d-51ad29a525c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}