{
    "id": "9e1a0124-475c-487a-91f2-4da88f4a37d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_powers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d3323ed-40b7-4c9e-9f5f-6527fdabf676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e1a0124-475c-487a-91f2-4da88f4a37d1",
            "compositeImage": {
                "id": "73d9082e-ba81-4a6f-959d-30ad624e8ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d3323ed-40b7-4c9e-9f5f-6527fdabf676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "590f9b90-a079-4071-ab1f-c273e49fdec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d3323ed-40b7-4c9e-9f5f-6527fdabf676",
                    "LayerId": "a51edc5e-7540-43b1-9414-1628adabd253"
                }
            ]
        },
        {
            "id": "1c0a1819-53b1-4602-97b1-8b400d939967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e1a0124-475c-487a-91f2-4da88f4a37d1",
            "compositeImage": {
                "id": "d0698f82-a4f2-479a-a336-b53f5b054f73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c0a1819-53b1-4602-97b1-8b400d939967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7ca97c-f1e0-4d9e-a274-a90c2f5319ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0a1819-53b1-4602-97b1-8b400d939967",
                    "LayerId": "a51edc5e-7540-43b1-9414-1628adabd253"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "a51edc5e-7540-43b1-9414-1628adabd253",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e1a0124-475c-487a-91f2-4da88f4a37d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}