/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 0186C7B5
instance_destroy();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4E441518
/// @DnDArgument : "var" "image_index"
if(image_index == 0)
{
	/// @DnDAction : YoYo Games.Instances.Set_Instance_Var
	/// @DnDVersion : 1
	/// @DnDHash : 2539AC11
	/// @DnDApplyTo : other
	/// @DnDParent : 4E441518
	/// @DnDArgument : "value" "1.5"
	/// @DnDArgument : "instvar" "15"
	with(other) {
	image_xscale = 1.5;
	}

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 25A6945D
	/// @DnDApplyTo : other
	/// @DnDParent : 4E441518
	/// @DnDArgument : "steps" "10 * room_speed"
	with(other) {
	alarm_set(0, 10 * room_speed);
	
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 093BF70C
else
{
	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 791F762E
	/// @DnDParent : 093BF70C
	/// @DnDArgument : "speed" "spd"
	speed = spd;
}