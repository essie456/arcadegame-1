{
    "id": "dcaf6d8c-95b7-48f9-9ee8-4baad3683f10",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_powers",
    "eventList": [
        {
            "id": "569877e3-9f89-40ea-a14a-3ec877da877e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dcaf6d8c-95b7-48f9-9ee8-4baad3683f10"
        },
        {
            "id": "0913f751-b12d-4951-99dc-1dd9cbf81122",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "dcaf6d8c-95b7-48f9-9ee8-4baad3683f10"
        },
        {
            "id": "45fe8016-b247-4650-8176-374775c4fca5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dcaf6d8c-95b7-48f9-9ee8-4baad3683f10"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9e1a0124-475c-487a-91f2-4da88f4a37d1",
    "visible": true
}