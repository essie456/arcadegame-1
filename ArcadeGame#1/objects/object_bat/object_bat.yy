{
    "id": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bat",
    "eventList": [
        {
            "id": "089d5f32-cdae-4969-bc92-e50f6bfaf7f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d"
        },
        {
            "id": "699f60bb-fc60-4878-8137-90acc055acf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d"
        },
        {
            "id": "07c05133-e897-4bff-a460-9d201f925eff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d"
        },
        {
            "id": "b081a793-ced5-449a-bba2-14138a2c154c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47c431bb-1cd3-49c7-9683-15457bd7f2e4",
    "visible": true
}