{
    "id": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_ball",
    "eventList": [
        {
            "id": "e9e79942-d91c-4bb1-a9f2-e47a8f8fc549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381"
        },
        {
            "id": "e255da01-b21b-43bc-9016-9ec50caaccfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381"
        },
        {
            "id": "53507e27-1fb6-4c08-948e-17e4c4e05e99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381"
        },
        {
            "id": "6525ac25-de98-4e0d-8a41-b95e5ea7c78d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7a04505d-93bb-4a0f-839b-899bd6e5ae9d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381"
        },
        {
            "id": "e4bd053e-8ac6-4cde-b931-dedc83641684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1d501e77-62ee-43ca-a1d8-001e7df3b7b9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381"
        },
        {
            "id": "60ef8e40-6be4-4ec4-a9ba-6c6a5701c96f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "e0dac5e2-6c42-4ed4-8702-a7eb81d04381"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3536f3d4-87d2-4bad-92a9-60cb5518956a",
    "visible": true
}