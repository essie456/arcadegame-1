{
    "id": "1d501e77-62ee-43ca-a1d8-001e7df3b7b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_brick",
    "eventList": [
        {
            "id": "0ea485a3-6bea-48c7-bef4-b37a6e9bf00d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d501e77-62ee-43ca-a1d8-001e7df3b7b9"
        },
        {
            "id": "b65ada31-77ae-42c8-ac2b-b29ef4ee67e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1d501e77-62ee-43ca-a1d8-001e7df3b7b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "df17f0a1-78bd-45b5-a54d-51ad29a525c4",
    "visible": true
}