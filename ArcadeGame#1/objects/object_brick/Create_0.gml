/// @DnDAction : YoYo Games.Random.Randomize
/// @DnDVersion : 1
/// @DnDHash : 47AF7923
randomize();

/// @DnDAction : YoYo Games.Random.Choose
/// @DnDVersion : 1
/// @DnDHash : 743A978C
/// @DnDInput : 6
/// @DnDArgument : "var" "colour"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "option" "c_red"
/// @DnDArgument : "option_1" "c_orange"
/// @DnDArgument : "option_2" "c_green"
/// @DnDArgument : "option_3" "c_yellow"
/// @DnDArgument : "option_4" "c_blue"
/// @DnDArgument : "option_5" "c_purple"
var colour = choose(c_red, c_orange, c_green, c_yellow, c_blue, c_purple);

/// @DnDAction : YoYo Games.Instances.Set_Instance_Var
/// @DnDVersion : 1
/// @DnDHash : 7688EF2B
/// @DnDArgument : "value" "colour"
/// @DnDArgument : "instvar" "14"
image_blend = colour;