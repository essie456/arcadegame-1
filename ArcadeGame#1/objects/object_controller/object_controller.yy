{
    "id": "a5981946-842a-4e4c-b5ea-10a3a30c4751",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_controller",
    "eventList": [
        {
            "id": "46a80762-e163-405d-bbfc-f919f73fe2a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "a5981946-842a-4e4c-b5ea-10a3a30c4751"
        },
        {
            "id": "8f3a4e8e-93d8-49b0-9deb-94a40255a8f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a5981946-842a-4e4c-b5ea-10a3a30c4751"
        },
        {
            "id": "678a37ea-0f6a-4c7b-bcb9-2e256916678b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5981946-842a-4e4c-b5ea-10a3a30c4751"
        },
        {
            "id": "bfe2f282-edc3-45d7-ac19-a8fb78b6cf1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a5981946-842a-4e4c-b5ea-10a3a30c4751"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}