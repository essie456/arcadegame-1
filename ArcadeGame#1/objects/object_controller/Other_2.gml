/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 7C5CC523
/// @DnDArgument : "font" "font_game"
draw_set_font(font_game);

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 41D83BCB
/// @DnDInput : 3
/// @DnDArgument : "value_2" "3"
/// @DnDArgument : "var" "player_score"
/// @DnDArgument : "var_1" "high_score"
/// @DnDArgument : "var_2" "player_lives"
global.player_score = 0;
global.high_score = 0;
global.player_lives = 3;