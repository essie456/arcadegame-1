/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 25D730B9
/// @DnDArgument : "x" "8"
/// @DnDArgument : "y" "8"
/// @DnDArgument : "caption" ""Score:""
/// @DnDArgument : "var" "global.player_score"
draw_text(8, 8, string("Score:") + string(global.player_score));

/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 3E9D011C
/// @DnDArgument : "halign" "fa_right"
draw_set_halign(fa_right);
draw_set_valign(fa_top);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 3F3FCF66
/// @DnDArgument : "x" "room_width-8"
/// @DnDArgument : "y" "8"
/// @DnDArgument : "caption" ""HighScore: ""
/// @DnDArgument : "var" "global.high_score"
draw_text(room_width-8, 8, string("HighScore: ") + string(global.high_score));

/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 1A594083
draw_set_halign(fa_left);
draw_set_valign(fa_top);

/// @DnDAction : YoYo Games.Drawing.Draw_Sprites_Stacked
/// @DnDVersion : 1
/// @DnDHash : 56306188
/// @DnDArgument : "x" "(room_width/2) -  ((global.player_lives -1) * 32)"
/// @DnDArgument : "y" "room_height-32"
/// @DnDArgument : "sprite" "sprite_lives"
/// @DnDArgument : "number" "global.player_lives"
/// @DnDSaveInfo : "sprite" "ea6d4dec-632f-48c3-9a13-dc846b9d0b70"
var l56306188_0 = sprite_get_width(sprite_lives);
var l56306188_1 = 0;
for(var l56306188_2 = global.player_lives; l56306188_2 > 0; --l56306188_2) {
	draw_sprite(sprite_lives, 0, (room_width/2) -  ((global.player_lives -1) * 32) + l56306188_1, room_height-32);
	l56306188_1 += l56306188_0;
}