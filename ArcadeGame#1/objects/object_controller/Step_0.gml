/// @DnDAction : YoYo Games.Instances.Instance_Get_Count
/// @DnDVersion : 1
/// @DnDHash : 75398688
/// @DnDArgument : "var" "numOfBricks"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "object" "object_brick"
/// @DnDSaveInfo : "object" "1d501e77-62ee-43ca-a1d8-001e7df3b7b9"
var numOfBricks = instance_number(object_brick);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 09B8ACB2
/// @DnDArgument : "var" "numOfBricks"
/// @DnDArgument : "op" "3"
if(numOfBricks <= 0)
{
	/// @DnDAction : YoYo Games.Rooms.Restart_Room
	/// @DnDVersion : 1
	/// @DnDHash : 24007ADA
	/// @DnDParent : 09B8ACB2
	room_restart();
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 088C0425
/// @DnDArgument : "var" "gameover"
/// @DnDArgument : "value" "true"
if(gameover == true)
{
	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
	/// @DnDVersion : 1
	/// @DnDHash : 08A0D5BB
	/// @DnDParent : 088C0425
	/// @DnDArgument : "key" "vk_anykey"
	var l08A0D5BB_0;
	l08A0D5BB_0 = keyboard_check_pressed(vk_anykey);
	if (l08A0D5BB_0)
	{
		/// @DnDAction : YoYo Games.Common.Set_Global
		/// @DnDVersion : 1
		/// @DnDHash : 31BC9D99
		/// @DnDParent : 08A0D5BB
		/// @DnDArgument : "var" "player_score"
		global.player_score = 0;
	
		/// @DnDAction : YoYo Games.Common.Set_Global
		/// @DnDVersion : 1
		/// @DnDHash : 721B57D0
		/// @DnDParent : 08A0D5BB
		/// @DnDArgument : "value" "3"
		/// @DnDArgument : "var" "player_lives"
		global.player_lives = 3;
	
		/// @DnDAction : YoYo Games.Rooms.Restart_Room
		/// @DnDVersion : 1
		/// @DnDHash : 32881750
		/// @DnDParent : 08A0D5BB
		room_restart();
	}
}