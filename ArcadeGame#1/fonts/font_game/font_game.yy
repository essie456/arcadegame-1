{
    "id": "956f289a-2eef-4c90-a23d-44b0aa9f713f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_game",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Jokerman",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4de0aca9-0ebd-4272-9ecf-189da81fc745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e57ef2ce-a200-4e84-a442-66deb21afdf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 10,
                "y": 80
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "87adb6b5-e439-4118-8fda-0daa89dedece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "47e08a7d-4d85-43dd-994d-356be2eaa6cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 240,
                "y": 54
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "64721615-a361-46c5-8e98-6a7b5f02ab0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 230,
                "y": 54
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7265ee1b-1cde-4a3b-95f3-5c1c2fc44add",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 212,
                "y": 54
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "480c8aa3-b8d8-4c51-97fe-d1a2874906bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 200,
                "y": 54
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0faf64d1-a247-48ea-973d-21e4e284547c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 194,
                "y": 54
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "524d4f07-40ab-485f-b676-3fa86314b6ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 186,
                "y": 54
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f4b2e8a7-2c7b-40fb-abae-73bf04f5e5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 178,
                "y": 54
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1d157159-67db-40b2-b019-803e1c3ed6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 16,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a636cc7f-76b0-4242-a94a-cee8e5def0e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 168,
                "y": 54
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cc30b3e7-b752-4667-aae3-36c32fef3c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 149,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "99b3796d-7b44-4689-a032-412a46f6e2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 141,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "88a8a0fa-f09e-4270-b571-ffecb177aa6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 135,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1f1d6e3c-49d9-42a8-9b48-89066c1febd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1a4f5ad5-cb21-44ee-a959-e7950e7c3ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 109,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8f7abdb6-d8b1-44bf-b3d3-f07d6ba32588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": -1,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e57bab87-e7b9-4889-96f9-d17e9c82d8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4db45504-2afe-4e45-a869-42bf0599540d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 77,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "90c75ea2-fef8-4166-a5f3-f944d4ed6bd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 63,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1e4eda4c-cc21-4198-b41b-81ea54e4def3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "af98d78b-ed91-4b1f-944f-f3f4d52f345f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e513e52a-37f2-49b6-8e9b-4d96d8bed0cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 37,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "36ed38df-6fc1-485f-bc1e-f6e2bff46b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 49,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d460805c-e567-4b39-881b-abf1690e8026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 44,
                "y": 106
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8bbb8a1e-82aa-4a53-86f5-503184f5b81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "069f17d6-4a4b-46a9-9b85-31c386f7b28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 31,
                "y": 106
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4ba03cca-ad59-4bac-8a7b-71a42e6db59a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 106
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "640fcddf-b632-4201-9ff4-e670d0f50bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 106
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fea55843-9f1c-4b7b-842e-02b616462a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a989cc02-fab4-4ab8-a1e6-50dcecad16ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 240,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5cc61d75-a4ee-4173-892e-fd4fcf0ef533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 225,
                "y": 80
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fa230aaa-977c-441a-9702-99a6a72617d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 210,
                "y": 80
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5a0b5204-ca3f-4841-b7c9-bbd70f1af0d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 198,
                "y": 80
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "32b886a0-dc78-4b07-aca0-f317d1d69af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 184,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "366d6dac-b899-400f-926d-8eba815581cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 172,
                "y": 80
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "88297860-b767-4dd1-aaa8-2aa08a7cb376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 161,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "efad2f33-2923-414c-8f52-86e8f43b02a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "66dde5b8-4175-4460-a249-292bb13abe99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 136,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "db2aa065-60b5-4e77-bcad-233525bf57f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 125,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1f4a9af8-7214-4eea-a6b2-f9056a63c79e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 118,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b3884225-158c-42d6-93d5-e37cc8244313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 105,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5a7d711f-9580-4152-af11-2d222203d5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 92,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2ecbc8a4-8c10-4c0a-aabf-9859566aaf99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 80,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e8d66b2b-8634-4a44-b9a4-da77b703c4eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 61,
                "y": 80
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b66f4b76-2f6e-48cb-acc8-03f7be79ebd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 48,
                "y": 54
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "02ed6468-670f-40f7-aae5-5b4c698578d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 34,
                "y": 54
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "028c6cc7-9c23-4d3a-acc9-36a957a61abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 21,
                "y": 54
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6b0a74de-1b6c-47e8-a88e-9d4cffe1a413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 12,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a28f4112-1202-4a36-a179-4d217bf7c628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3bf98ddd-5179-48ff-99d8-3db50147219b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4d5c8d72-0882-4959-a081-e77c718c4490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "12d2f360-ce2b-403c-a35c-5bdb49c79da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a922aedf-c6e6-4b20-a0ce-e45e09635b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c7a2adc3-5d11-4725-8135-4c6b627ac80f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 19,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "23ef2e07-ca4f-443b-acd4-e20b44fa5b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6b8331de-7a6c-48b4-a8b4-1eaced577628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "26bb1f5d-efa2-43b8-a0af-cb2321600962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b8bab3fa-92a1-4a5b-95f7-287d2cfa3ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6cf8e214-c2d9-4c00-87a6-379afc820410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "daa9584a-a24f-4f0d-919d-6e94bbcbbd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "09304d66-6480-4fe1-9844-5b3dbe1b515d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bcab6361-c60d-4fd2-a5b3-a7ba38d4350c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "659d2f71-4b6b-43b8-bcbd-49da95f064c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f0c2abe5-0ed9-4b76-9827-e30dee4e5e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7fd605b9-baab-4a47-b1e6-8e7af3f9fcba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f900200d-bfa2-4dcb-8bf7-3d6d90d407cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "371bffcd-7263-48c0-9a00-8c22b7ae8b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "07e70d55-09e1-42a7-b14e-c4844cdd9bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5f01394d-6f3e-4f19-befc-fa9ee5191a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "611f98de-ff01-4150-8f59-f77c134c0e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 28,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4b5cf334-f0bc-4e0b-97dc-f25851ea4be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 145,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "33ccfb3f-797b-4849-8937-554d85704c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 39,
                "y": 28
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4394c6ca-f014-4ceb-8aff-bd98f2c15df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": -3,
                "shift": 5,
                "w": 8,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "405bd77c-a47c-43d1-9784-b8382209c8e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 245,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c812dbf9-bf46-49da-a47f-7f27c7ddaf79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 28
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "51b481c2-e945-4601-b143-9cfd7225837f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 223,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2d50d0c9-de31-46ff-9a59-dbf9f1bb1b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 28
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4660169c-9ce6-4a41-acef-30832538f598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 199,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fdc257da-35ce-4623-85e9-3583f49c6133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 187,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "841b6de7-2530-4555-9388-685706e9b806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 176,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2e720922-0f17-4546-99c2-325f11460899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 165,
                "y": 28
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bafe9048-8395-4e37-814f-758c4939297b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 12,
                "y": 54
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4f971efd-4179-41fa-bd6a-40d4d628916a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 157,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "db6ede04-c7c6-445d-81c5-53cd6da1e79f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "990c65cb-6f35-4dfd-bb65-67d0563f1f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 122,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "73c8f00e-42bb-4df3-b0a7-ef0aa0701911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 108,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0073bbc0-a848-4791-b941-f3c9a5be3932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 96,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2eab6b81-7254-4070-acdb-838131670f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 85,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "086f5dbe-c96a-48b7-8ab0-ab53d194e163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 73,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "44ebdebc-56f2-460b-9538-1507876b4905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 62,
                "y": 28
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cd91ca0b-442b-4f91-8051-c655d32ec334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 57,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ed4bf488-7ddc-4482-9c20-2464f3cc4e32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 28
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f67f6277-c681-477a-861c-3148fbca3c80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 106
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "352af383-ba4d-4c05-8c37-d1e2f1d1615e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 24,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 68,
                "y": 106
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}